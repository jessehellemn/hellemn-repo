#!/bin/bash -xe
# Copyright 2019 Stem Inc.

# In general, this build script is just a lightweight proxy
# to the default build script provided by stem_ms_app

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# get the python site-packages dir e.g. /venv/lib/pythonX.X/site-packages
# snippet borrowed from SO...https://stackoverflow.com/a/122340
PYTHON_SITE_PACKAGES_DIR="$(python -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")"

# our default build script is installed as part of the stem_ms_app package
STEM_MS_BUILD_SH_PATH="$PYTHON_SITE_PACKAGES_DIR/stem_ms_app/build_tools/stem_ms_build.sh"

MICROSERVICE_DIR=$THIS_DIR \
    PYTHON_PKG_NAME="hello" \
    IMAGE_NAME="hello" \
    DOCKER_CONTAINER_NAME="hello" \
    $STEM_MS_BUILD_SH_PATH "$@"
