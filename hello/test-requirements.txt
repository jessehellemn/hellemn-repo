mock==2.0.0
pytest==3.4.0
pytest-xdist==1.22.0
pytest-cov==2.5.1
coverage==4.5.1
flake8==3.5.0
