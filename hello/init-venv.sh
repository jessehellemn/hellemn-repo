#!/bin/bash -xe

##########################################################################
# Create or activate a python virtualenv for this project
##########################################################################
set -e
virtualenv .venv-hello --python=python3
source .venv-hello/bin/activate
PIP_OPTIONS="--extra-index-url https://nexus.stem.com/repository/stem_py_services/simple"
.venv-hello/bin/python -m pip install -e . $PIP_OPTIONS
set +e
