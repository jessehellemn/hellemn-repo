import os

import pytest
from stem_ms_app.application_context import ApplicationContext
from stem_ms_app.svc.k8s import K8sService
from stem_ms_app.svc.vault_svc import VaultService


def pytest_addoption(parser):
    """
    Add command line option to pytest to be able to pass a configuration
    file for stem apps. The configuration file can contain environment
    specific information and credentials.
    """
    parser.addoption('--stem-ms-config',
                     action='store',
                     default=os.path.expanduser('~/.stem-ms/config/testenv.json'),
                     help='supplementary configuration properties for stem ms apps')


@pytest.fixture(scope='session')
def app_ctx(request):
    config_filename = request.config.getoption("--stem-ms-config")
    config_props = {
        'app.name': 'hello',
        'log.root_log_handler': 'consoleSimple',
        'log.root_log_level': 'DEBUG',
        'k8s.server': None,  # Get from env or config file
        'k8s.ssl_verify': None,  # Get from env or config file
        'k8s.namespace': None,  # Get from env or config file
        'k8s.credentials': {'token': None},  # Get from env or config file
        'app.host': 'stem-k8s://hello/8000/host',
        'app.port': 'stem-k8s://hello/8000/nodePort',
    }
    app_ctx = ApplicationContext().configure(
        app_name='integ_test',
        app_default_props=config_props,
        prop_filenames=[config_filename]
    )
    app_ctx.register_service(K8sService(), 'k8s')
    app_ctx.register_service(VaultService())

    return app_ctx
