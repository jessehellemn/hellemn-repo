import requests


def test_status(app_ctx):
    host = app_ctx.get_config_property('app.host')
    port = app_ctx.get_config_property('app.port')
    url = 'http://{}:{}'.format(host, port)
    with requests.get(url) as resp:
        assert(resp.status_code == 200)
