import unittest
import os

if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTests(unittest.TestLoader().discover(start_dir=os.path.dirname(__file__)))
    unittest.TextTestRunner().run(suite)
