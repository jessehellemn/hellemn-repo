import json

import falcon

from stem_ms_app.application_context import AbstractResource
from stem_ms_app.svc.http_security_decorators import requires_authentication, requires_permission


class MyResource(AbstractResource):
    """A simple status service to demo security related annotations.
    """

    def add_routes(self):
        self.add_route('/v1/hello/my_resource', self)

    @requires_authentication
    @requires_permission(None)
    def on_get(self, req, resp):
        """ Simple service """
        resp.status = falcon.HTTP_200

        response_body = {
            'response': {'code': 0, 'msg': 'success from my_resource'},
        }

        resp.body = json.dumps(response_body)
