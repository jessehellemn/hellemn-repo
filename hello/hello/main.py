# Imports
import logging

from stem_ms_app.application_context import ApplicationContext
from stem_ms_app.rs.status import StatusResource
from stem_ms_app.svc.jwt_svc import JwtService
from stem_ms_app.svc.vault_svc import VaultService
from stem_ms_app.svc.falcon_svc import FalconService
from stem_ms_app.svc.sql import SqlConnectionService

from hello.rs.my import MyResource

logging.basicConfig()
logger = logging.getLogger(__name__)

# Create application context
app_ctx = ApplicationContext()
app_ctx.configure(
    app_name='hello',
    app_default_props={
        'app.name': 'hello',
        'app.config_property_set_name': 'hello',
        'app.config_namespace': 'default',
        'log.root_log_level': 'DEBUG',
        'log.root_log_handler': 'consoleJson',
        'mysql_portal.url': 'mysql+mysqlconnector://${username}:${password}@${host}:${port}/${schema}',
        'mysql_portal.host': 'localhost',
        'mysql_portal.port': '3306',
        'mysql_portal.credentials': {'username': 'root', 'password': ''},
        'mysql_portal.schema': 'portal',
        'vault.url': 'http://localhost:8200',
        'vault.token': 'dev_vault_root_token',
        'vault.path_prefix': 'secret',
        'jwt.exp_seconds': '36000',
        'restapi.validation_enabled': 'True',
        'restapi.spec_pkg_name': 'hello',
        'restapi.spec_file_name': 'swagger.yml'
    }
)

# Register services
# Order of registration of services should take dependencies among services into account.
app_ctx.register_service(VaultService())
app_ctx.register_service(JwtService())
app_ctx.register_service(FalconService())
app_ctx.register_service(SqlConnectionService(), 'mysql.portal')

# Register REST resources
# This assumes that required services for e.g. the falcon service have been registered.
app_ctx.register_resource(StatusResource())
app_ctx.register_resource(MyResource())

# The application object that is needed by a WSGI server
falcon_app = app_ctx.get_falcon_app()

# Simple run using embedded server useful for debugging
if __name__ == '__main__':
    from wsgiref import simple_server
    httpd = simple_server.make_server('127.0.0.1', 8000, falcon_app)
    logger.info('Now serving on port 8000.')
    httpd.serve_forever()
