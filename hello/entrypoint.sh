#!/bin/bash
# Copyright 2019 Stem Inc.

if [ $# -ne 1 ]; then
  echo "Usage: ${0} start"
  exit 1
fi


if [ $1 = 'start' ]; then
    WKG_DIR=`dirname ${0}`
    echo "Setting working directory to ${WKG_DIR}"
    cd ${WKG_DIR}
    exec gunicorn hello.main:falcon_app --config gunicorn_config.py
fi
