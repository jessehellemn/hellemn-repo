# hello
Package hello created from the cookiecutter template template_restapi

## Setup python virtual environment
```
$ source ./init-venv.sh
```

See https://stemedge.atlassian.net/wiki/spaces/SW/pages/407404880/Project+Setup#ProjectSetup-LocalBuild for stem-builder setup and commands

## Build Python wheel
```
$ ./build.sh wheel 

or

$ stem-builder build-wheel
```

## Build docker image
```
$ ./build.sh docker
or
$ stem-builder build-docker
```

## Inspect for PEP8 compliance and Python 2/3 compatibility
```
$ ./build.sh inspect
or
$ stem-builder inspect-src
```

## Run unit tests
```
$ ./build.sh test
or
$ stem-builder coverage
```

## Run
### Run in a new docker container
```
$ ./build.sh docker
$ ./build.sh run
```

Once running you should be able to ping the endpoint 
```
$ curl -XGET http://localhost:8000/
```

### Run to debug a docker container
```
$ ./build.sh run_debug
```
This launches bash in the new container without running the entrypoint command.
You can look at the container contents and execute the startup command manually (`./entrypoint.sh start`)

### Run without using docker in a python virtualenv
```
$ ./build.sh run_no_docker
```
or
```
$ ./entrypoint.sh start
```
