# Copyright 2019 Stem Inc.

import os
from setuptools import setup, find_packages

# special version number for local builds
BUILD_VERSION = os.environ.get('BUILD_VERSION', '0.0.0.d0')

# see pip maintainer Donald Stufft's recommendations on
# requirements.txt vs setup.py's install_requires:
# https://caremad.io/posts/2013/07/setup-vs-requirement/
requirements = [
    'stem_ms_app==1.20.0-dev1',
]

setup(
    name='hello',
    description='Stem common application bootstrap and convenience libs',
    version=BUILD_VERSION,
    author='jessehellemn',
    license='Proprietary. Copyright 2019 Stem Inc.',
    packages=find_packages(),
    data_files=[],
    include_package_data=True,
    install_requires=requirements,
)
